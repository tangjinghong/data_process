#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
# @Description jieba分词操作详解
# @Time : 2020/1/4 22:00 
# @Author : sky 
# @Site :  
# @File : jiebaCut.py 
# @Software: PyCharm
"""

"""
支持三种分词模式与特点
1、精准模式：试图将句子最精确地切分开，适合文本分析
2、全模式：把句子中所有的可以成词的词语都扫描出来，速度非常快，但是不能解决歧义
3、搜索引擎模式：在精确模式的基础上，对长词可以再次切分，提高召回率，适合用于搜索引擎分词

支持繁体分词
支持自定义词典

安装方法
1、自动安装 pip install jieba
2、半自动安装：下载 http://pypi.python.org/pypi/jieba，解压后运行 python setup.py install
3、手动安装：将jieba目录放置于当前目录或sit-packages目录下

通过import jieba进行引用

核心算法
1、基于前缀词典实现搞笑的词图扫描，生成句子中汉字所有的可能成词的情况所构建成的有向无环图（DAG）
2、采用动态规划查找最大概率路径，找出基于词频的最大切分组合
3、对于未登录词，采用了基于汉字成词能力的HMM模型，使用了Viterbi算法

主要功能：
1、jieba.cut 三个输入参数：待分词的字符串；cut_all参数是否全模型；HMM参数是否引用HMM模型
2、jieba.cut_for_search 两个参数：待分词字符串，是否HMM模型。该方法适合用于搜索引擎构建倒排索引的分词，粒度比较细
3、jieba.lcut 以及 jieba.lcut_for_search 直接返回list
4、jieba.Tokenizer(dictionary=DEFAULT_DICT)：新建自定义分词器，可用于同时使用不同词典。jieba.dt为默认分词器
"""


import jieba

# ***********************************结巴分词的基本操作***********************************
print('-'*40)
print('分词')
print('-'*40)
# 全模式：把句子中所有的可以成词的词语都扫描出来，速度非常快，但是不能解决歧义
seg_list = jieba.cut('我来到北京的北京大学', cut_all=True, HMM=True)
print('Full Mode', ' '.join(seg_list))

# 默认是精确模式
# 精准模式：试图将句子最精确地切分开，适合文本分析
seg_list = jieba.cut('我来到北京的北京大学')
print('Default Mode', ' '.join(seg_list))

# 搜索引擎模式：在精确模式的基础上，对长词可以再次切分，提高召回率，适合用于搜索引擎分词
seg_list = jieba.cut_for_search('我来到北京的北京大学', HMM=False)
print('Search Mode', ' '.join(seg_list))

# ***********************************调整词典***********************************
print('-'*40)
print('调整词典')
print('-'*40)

seg_list = jieba.cut('如果放到数据库中将出错')
print('原文档： \t' + '/'.join(seg_list))
print(jieba.suggest_freq(('中', '将'), True))
seg_list = jieba.cut('如果放到数据库中将出错')
print('改进文档： \t' + '/'.join(seg_list))

seg_list = jieba.cut('台中正常的话不应该被拆分')
print('原文档： \t' + '/'.join(seg_list))
print(jieba.suggest_freq(('台中'), True))
seg_list = jieba.cut('台中正常的话不应该被拆分')
print('改进文档： \t' + '/'.join(seg_list))

# ***********************************加载自定义词典***********************************
print('-'*40)
print('添加用户自定义词典')
print('-'*40)

seg_list = jieba.cut('今天很高兴和大家在数据预处理专题和大家进行交流学习')
print('未使用自定义分词词典： \t' + '/'.join(seg_list))

jieba.load_userdict('../dataset/word_dict/user_dict.txt')

seg_list = jieba.cut('今天很高兴和大家在数据预处理专题和大家进行交流学习')
print('使用自定义分词词典： \t' + '/'.join(seg_list))

# ***********************************关键词提取***********************************

print('-'*40)
print('关键词提取')
print('-'*40)
print('TF-IDF')
print('-'*40)

"""
extract_tags(sentence, topK=20, withWeight=False, allowPOS=())
sentence 为待提取的文本
topK 返回几个TF-IDF 权重最大的关键词，默认值为20
withWeight 是否一并返回关键词权重，默认为False
allowPOS 仅包含制定词性的词，默认为空，即不筛选

jieba.analyse.TFIDF(idf_path=None) 新建 TFIDF 实例，idf_path 为
"""

import jieba.analyse
text = '此外，公司拟对全资子公司吉林欧亚置业有限公司增资4.3亿元，增资后，吉林欧亚置业注册资本由7000万元增加到5亿元。' \
       '吉林欧亚置业主要经营范围为房地产开发及百货零售等业务。目前在建吉林欧亚城市商业综合体项目。2013年，' \
       '实现营业收入0万元，实现净利润-139.13万元。'
for x, w in jieba.analyse.extract_tags(text, 10, withWeight=True):
    print(x, w)


print('-'*40)
print('TextRank')
print('-'*40)

"""
textrank(self, sentence, topK=20, withWeight=False, allowPOS=('ns', 'n', 'vn', 'v'))
与extract_tags接口相同，直接使用，有默认词性
"""
for x, w in jieba.analyse.textrank(text, 10, withWeight=True):
    print(x, w)

print('-'*40)
print('词性标注')
print('-'*40)

import jieba.posseg

words = jieba.posseg.cut('我爱北京天安门')
for word, flag in words:
    print(word, flag)

print('-'*40)
print('Tokenize：返回词语在原文的起止位置')
print('-'*40)
print('默认模式')
print('-'*40)

result = jieba.tokenize('永和服装饰品有限公司')
for tk in result:
    # 含头不含尾
    print('word %s\t\t start:%d\t\t end:%d' % (tk[0], tk[1], tk[2]))

print('-'*40)
print('搜索模式')
print('-'*40)

result = jieba.tokenize('永和服装饰品有限公司', mode='search')
for tk in result:
    # 含头不含尾
    print('word %s\t\t start:%d\t\t end:%d' % (tk[0], tk[1], tk[2]))
