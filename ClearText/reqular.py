#!/usr/bin/env python
# -*- encoding: utf-8 -*-
"""
# @Description 正则表达式学习
# @Time : 2020/1/4 15:13 
# @Author : sky 
# @Site :  
# @File : reqular.py 
# @Software: PyCharm
"""

import re


# . 任意字符
# * 任意次数
# ^ 表示开头
# $ 表示结尾
# ? 非贪婪模式，提取第一个字符
# + 至少出现一次
# {1}出现一次
# {3,}出现3次以上
# {2,5}最少2次，最多5次
# \d 匹配数字
# [\u4E00-\u9FA5] 匹配汉字
# | 或
# [] 满足任意一个， [2345] 2345中任意一个 [0-9]区间 [^1]非1
# \s 空格 \S 非空格
# \w 匹配[A-Za-z0-9_] \W

text = 'this is Python 数据预处理， 这次学习的很好，使用的环境是Anaconda4.4，现在的时间是2020年1月4日'

# 开头 + 任意字符 + 任意次数
reg_1 = '(^t.*)'
# 存在s 这样会一直匹配到最后一个s
reg_2 = '.*(s+)'
# 存在s 贪婪 匹配到第一个s
reg_3 = '.*?(s+)'
# 匹配汉字?的作用同上
reg_4 = '.*?([\u4E00-\u9FA5]+的)'
# 匹配日期
reg_5 = '.*(\d{4}年)(\d{1,2}月)'

res = re.match(reg_5, text)
if res:
    print(res)
    print(res.group(2))  # group对应的数字是正则中括号的内容
else:
    print('没匹配到')

# 日期的提取
print('-'*20)

date_text = '现在的日期是2020年1月4日'
# date_text = '现在的日期是2020年01月04日'
# date_text = '现在的日期是2020-1-4'
# date_text = '现在的日期是2020-01-04'
# date_text = '现在的日期是2020/1/4'
# date_text = '现在的日期是2020/01/04'
# date_text = '现在的日期是2020-01'

reg_date = r'.*(\d{4}[年/-]\d{1,2}[月/-]\d{,2}[日]?)'
res = re.match(reg_date, date_text)
if res:
    print(res)
    print(res.group(1))  # group对应的数字是正则中括号的内容
else:
    print('没匹配到')

# 手机号的提取
print('-'*20)

# phone_text = '我的手机号是13030010152，有什么问题可以联系我'
# phone_text = '我的手机号是17091033442，有什么问题可以联系我'
# phone_text = '我的手机号是18519299012，有什么问题可以联系我'
phone_text = '我的手机号是13691769664 ，有什么问题可以联系我'

reg_phone = r'.*?(1[37859]\d{9})'
res = re.match(reg_phone, phone_text)
if res:
    print(res)
    print(res.group(1))  # group对应的数字是正则中括号的内容
else:
    print('没匹配到')
