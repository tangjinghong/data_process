# coding=utf-8

"""
Description: Word文档信息提取
"""
import fnmatch
import os

from win32com import client


def word2txt(file_path, save_path=''):
    """
    功能描述：Word文件转存txt，默认保存在根目录下，支持自定义
    参数描述：1 filepPath 文件路径 2 savePath 保存路径
    """

    # 切分文件路径为文件目录和文件名
    dirs, filename = os.path.split(file_path)
    print(dirs, '-----------', filename)

    # 修改切分后的文件后缀
    if fnmatch.fnmatch(filename, '*.doc'):
        new_name = filename[:-4] + '.txt'
    elif fnmatch.fnmatch(filename, '*.docx'):
        new_name = filename[:-5] + '.txt'
    else:
        print('格式有误，仅支持doc/docx格式')
        return

    # 设置新的文件保存路径
    if not save_path:
        save_path = dirs
    else:
        save_path = save_path

    txt_path = os.path.join(save_path, new_name)
    print('保存路径-->', txt_path)

    # 加载文本处理的处理程序，实现Word --> txt
    print('开始转换...')
    # 使用时根据文件默认打开方式进行选择
    word_app = client.Dispatch('Word.Application')  # office
    # word_app = client.Dispatch("KWPS.Application")  # WPS
    txt = word_app.Documents.Open(file_path)
    print('转换完成...')

    # 保存
    print('开始保存...')
    txt.SaveAs(txt_path, 4)  # 参数4代表抽取文本
    print('保存完成...')
    txt.Close()


if __name__ == '__main__':
    word_file_path = os.path.abspath(r'../dataset/Corpus/word/6任务书参考模板.docx')
    word2txt(word_file_path)
