# coding=utf-8

"""
Description：批量处理文件
"""
import os

from ExtractText.ExtractTxt import file2txt


def get_all_file(root_path):
    """
    获取目录下所有文件
    :param root_path: 文件目录
    :return: 目录下所有文件
    """
    all_file = []
    # 如果传入的是文件，那么直接返回
    if os.path.isfile(root_path):
        all_file.append(root_path)
    # 如果传入的是目录，遍历递归目录，获取所有文件
    elif os.path.isdir(root_path):
        child_file = os.listdir(root_path)
        for file in child_file:
            path = os.path.join(root_path, file)
            if os.path.isfile(path):
                all_file.append(path)
            # 如果自文件为目录，则递归处理
            elif os.path.isdir(path):
                all_file.extend(get_all_file(path))
    return all_file


if __name__ == '__main__':
    all_file = get_all_file('..\\dataset')

    for file in all_file:
        file_path = os.path.abspath(file)
        print(file_path)
        file2txt(file_path)
