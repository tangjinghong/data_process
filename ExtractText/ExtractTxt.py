# coding=utf-8

"""
Description：多格式文档文本抽取工具
"""
import fnmatch
import os

from win32com import client


def file2txt(file_path, save_path=''):
    """
    功能描述：文件转存txt，默认保存在根目录下，支持自定义
    :param file_path: 文件路径
    :param save_path: 保存路径
    """
    # 切分文件路径为文件目录和文件名
    dirs, filename = os.path.split(file_path)
    print(dirs, '-----------', filename)

    # 修改切分后的文件后缀
    # 获取文件后缀
    typename = os.path.splitext(filename)[-1].lower()
    new_name = tran_type(filename, typename)

    # 设置新的文件保存路径
    if not save_path:
        save_path = dirs
    else:
        save_path = save_path

    txt_path = os.path.join(save_path, new_name)
    print('保存路径-->', txt_path)

    # 加载文本处理的处理程序，实现Word --> txt
    print('开始转换...')
    # 使用时根据文件默认打开方式进行选择
    word_app = client.Dispatch('Word.Application')  # office
    # word_app = client.Dispatch("KWPS.Application")  # WPS
    txt = word_app.Documents.Open(file_path)
    print('转换完成...')

    # 保存
    print('开始保存...')
    txt.SaveAs(txt_path, 4)  # 参数4代表抽取文本
    print('保存完成...')
    txt.Close()


def tran_type(filename, typename):
    """
    根据文件后缀修改文件名
    :param filename: 文件名
    :param typename: 后缀名
    :return: 新文件名
    """
    new_filename = ''
    if typename == '.pdf':
        # pdf --> txt
        if fnmatch.fnmatch(filename, '*.pdf'):
            new_filename = filename[:-4] + '.txt'
        else:
            return
    elif typename == '.doc' or typename == '.docx':
        # word --> txt
        if fnmatch.fnmatch(filename, '*.doc'):
            new_filename = filename[:-4] + '.txt'
        elif fnmatch.fnmatch(filename, '*.docx'):
            new_filename = filename[:-5] + '.txt'
        else:
            return
    # 其他后缀情况自行判断
    # elif typename == '.ppt':
    #     pass
    else:
        print('输入数据不合法')

    return new_filename


if __name__ == '__main__':
    word_file_path = os.path.abspath(r'../dataset/Corpus/word/6任务书参考模板.docx')
    file2txt(word_file_path)
